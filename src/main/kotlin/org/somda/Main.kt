package org.somda

import com.akuleshov7.ktoml.Toml
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.serializer
import org.somda.protosdc.sdc.dpws.DpwsConfig
import org.somda.protosdc.sdc.dpws.dagger.DaggerDpwsComponent
import java.io.File
import java.io.IOException
import kotlin.system.exitProcess

fun main() {
    val tomlContent = File("app-config.toml").readText()

    val dpwsConfig = Toml.partiallyDecodeFromString<DpwsConfig>(serializer(), tomlContent, "DpwsConfig")

    val component = DaggerDpwsComponent.builder().bind(dpwsConfig).build()
    val discoProxyComponent = component.discoveryProxyComponentBuilder()
        .build()

    val discoProxy = discoProxyComponent.discoveryProxy()

    runBlocking {
        discoProxy.apply {
            println("Start discovery proxy")
            start()
            println("Discovery proxy started. Connectors:")
            println("- HTTP:  ${discoProxy.httpConnectorUrl() ?: "none"}")
            println("- HTTPS: ${discoProxy.httpsConnectorUrl() ?: "none"}")
        }
    }

    Runtime.getRuntime().addShutdownHook(Thread {
        runBlocking {
            println("Shutdown received")
            discoProxy.stop()
            println("Discovery proxy shut down")
        }
    })
}