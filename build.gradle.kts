import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    val kotlinVersion = "2.0.20"
    java
    idea
    application
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.serialization") version kotlinVersion
    alias(libs.plugins.org.somda.append.snapshot.id)
    alias(libs.plugins.org.somda.gitlab.maven.publishing)
    alias(libs.plugins.org.somda.repository.collection)
}

group = "org.somda"
version = "1.0.4"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.sdc.kt)
    implementation(libs.coroutines.core)
    implementation(libs.protosdc.crypto)
    implementation(libs.protosdc.common)
    implementation(libs.ktor.server.core)
    implementation(libs.ktor.server.jetty.jakarta)
    implementation(libs.ktor.client.core)
    implementation(libs.ktor.client.core)
    implementation(libs.ktor.client.apache)
    implementation(libs.ktor.client.encoding)
    implementation(libs.log4j)
    implementation(libs.log4j.api.kotlin)
    implementation(libs.slf4j.nop)
    implementation(libs.ktoml.core)
}

val javaSource: Int = 17
val jdkVersion: JvmTarget = JvmTarget.JVM_17

java {
    toolchain {
        languageVersion = JavaLanguageVersion.of(javaSource)
    }
    withSourcesJar()
}

kotlin {
    compilerOptions {
        jvmTarget = jdkVersion
    }
}

application {
    mainClass.set("org.somda.MainKt")
}

tasks {
    val fatJar = register<Jar>("fatJar") {
        // We need this for Gradle optimization to work
        dependsOn.addAll(listOf("compileJava", "compileKotlin", "processResources"))

        // Naming the jar
        archiveClassifier.set("standalone")

        duplicatesStrategy = DuplicatesStrategy.EXCLUDE

        // Provided we set it up in the application plugin configuration
        manifest { attributes(mapOf("Main-Class" to application.mainClass)) }
        val sourcesMain = sourceSets.main.get()
        val contents = configurations.runtimeClasspath.get()
            .map { if (it.isDirectory) it else zipTree(it) } +
                sourcesMain.output
        from(contents)

        // exclude security-related files from included libs as otherwise main may not start
        exclude("META-INF/*.RSA", "META-INF/*.SF", "META-INF/*.DSA")
    }
    build {
        // Trigger fat jar creation during build
        dependsOn(fatJar)
    }
}